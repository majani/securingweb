package com.security.example.securingweb.entity;

import org.springframework.security.crypto.password.PasswordEncoder;

import lombok.Data;

@Data
public class RegisterForm {

	private final String username;
	private final String password;
	private final String fullname;
	private final String street;
	private final String city;
	private final String state;
	private final String zip;
	private final String phoneNumber;

	public User toUser(PasswordEncoder paswordEncoder) {
		return new User(0, username, paswordEncoder.encode(password), fullname, street, city, state, zip, phoneNumber);

	}

}
