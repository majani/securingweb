package com.security.example.securingweb.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.security.example.securingweb.entity.User;

@Service
public class UserRepoUserDetailService implements UserDetailsService {

	@Autowired
	UserRepository userRepo;

	public UserRepoUserDetailService(UserRepository userRepo) {
		super();
		this.userRepo = userRepo;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepo.findByUsername(username);
		
		if(user != null) {
			return user;
		}
		
		throw new UsernameNotFoundException("User '" +username+ "' not found.");
	}

}
