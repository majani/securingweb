package com.security.example.securingweb.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.security.example.securingweb.data.PostRepository;
import com.security.example.securingweb.entity.Post;
import com.security.example.securingweb.entity.User;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/post")
public class PostController {
	

	@Autowired
	PostRepository postRepo;

	@GetMapping("/add")
	public String showForm() {
		return "post";

	}

	@PostMapping("/add")
	public String addPost(Post post, Errors errors, @AuthenticationPrincipal User user) {

		if (errors.hasErrors()) {
			log.info("ERRORS");
			return "post";
		}

		log.info("SUBMITTING POST");
		post.setUser(user);
		postRepo.save(post);
		return "redirect:/hello";

	}

//	@GetMapping("/list")
//	public String listPosts() {
//		
//	}

}
